﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            //Diwik diwikObj = new Diwik();
            //diwikObj.TestMethodDiwik();

            //Tanush tanushObj = new Tanush();
            //tanushObj.TestMethodTanush();
            //tanushObj.TestMethodDiwik();

            //Prajna prajnaObj = new Prajna();
            //prajnaObj.TestMethodDiwik();
            //prajnaObj.TestMethodTanush();
            //prajnaObj.TestMethodPrajna();

            Presty prestyObj = new Presty();
            prestyObj.TestMethodPresty();
            prestyObj.TestMethodDiwik();

        }
    }


    //inheritance- child will receive from parent (here child can access parent test methods,but parent cannot access child methods)
    public class Diwik
    {
        public void TestMethodDiwik()
        {
            Console.WriteLine("diwik method");
        }
    }

    public class Tanush:Diwik  //single inheritance
    {
        public void TestMethodTanush()
        {
            Console.WriteLine("Tanush method");
        }
    }

    public class Prajna:Tanush  //multilevel inheritance
    {
        public void TestMethodPrajna()
        {
            Console.WriteLine("Prajna method");
        }
    }

    public class Presty:Diwik  //hierarchy inheritance
    {
        public void TestMethodPresty()
        {
            Console.WriteLine("Presty method");
        }
    }

}
